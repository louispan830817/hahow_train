import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Button,
    TextInput,
    TouchableHighlight
} from 'react-native';
import MyButton from './src/button/myButton';
export default function prop_text() {
    const printMyButton = () => {
        console.log('按到按鈕了');
    }
    return (
        <View style={styles.container}>
            <Text>prop_概念測試</Text>
            <MyButton
                backgroundColor={'red'} color={'yellow'} myTitle={'prop'} myOnPress={() => printMyButton()} />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
})