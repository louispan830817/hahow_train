import React, { useState } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Button,
    TextInput,
    TouchableHighlight
} from 'react-native';
export default function myButton(props) {
    const bkcolor = props.backgroundColor;
    const btncolor = props.color;
    console.log(bkcolor)
    return (
        <TouchableHighlight onPress={props.myOnPress}
            underlayColor={bkcolor}
            style={{
                color: btncolor
            }}>
            <Text style={{
                color: btncolor, fontWeight: 'bold',
                fontSize: 30
            }}>{props.myTitle}</Text>
        </TouchableHighlight >
    )
}